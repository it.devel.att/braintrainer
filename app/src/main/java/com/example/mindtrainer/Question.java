package com.example.mindtrainer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Question {
    private int firstNumber;
    private int secondNumber;
    private float rightResult;

    private static final String[] signs = new String[]{"+", "-", ":", "*"};

    private String exampleStringFmt = "%s %s %s = ?";
    private String exampleString;

    private Random random = new Random();
    private List<Integer> usesWrongNumbers = new ArrayList<>();

    public Question() {
        generateExample();
    }

    private void generateExample() {
        firstNumber = random.nextInt(9);
        secondNumber = random.nextInt(9);
        switch (signs[random.nextInt(signs.length - 1)]) {
            case "+":
                rightResult = firstNumber + secondNumber;
                exampleString = String.format(exampleStringFmt, firstNumber, "+", secondNumber);
                break;
            case "-":
                rightResult = firstNumber - secondNumber;
                exampleString = String.format(exampleStringFmt, firstNumber, "-", secondNumber);
                break;
            case "*":
                rightResult = firstNumber * secondNumber;
                exampleString = String.format(exampleStringFmt, firstNumber, "*", secondNumber);
                break;
            case ":":
                generateSecondNumberNotZeroRandomInt();
                rightResult = firstNumber / (float) secondNumber;
                exampleString = String.format(exampleStringFmt, firstNumber, "/", secondNumber);
                break;
        }
    }

    public String getExampleString() {
        return exampleString;
    }

    public float getRightResult() {
        return (float) (Math.round(rightResult * 100) / 100.0);
    }

    public Integer getWrongAnswer() {
        Integer wrongNumber = random.nextInt((int) rightResult + 10) - 10;
        while ((wrongNumber == rightResult) || usesWrongNumbers.contains(wrongNumber)) {
            wrongNumber = random.nextInt((int) rightResult + 10) - 10;
        }

        usesWrongNumbers.add(wrongNumber);
        return wrongNumber;
    }

    private void generateSecondNumberNotZeroRandomInt() {
        while (secondNumber == 0) {
            secondNumber = random.nextInt(10);
        }
    }

    public static Question[] generateExamples() {
        Question[] questions = new Question[10];

        for (int i = 0; i < 10; i++) {
            questions[i] = new Question();
        }

        return questions;
    }
}
