package com.example.mindtrainer;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private String TAG = getClass().getSimpleName();

    public TextView roundCounter;
    public TextView wrongAnswersCount;
    public TextView rightAnswersCount;
    public TextView mainExample;

    public Button buttonOne;
    public Button buttonTwo;
    public Button buttonThree;
    public Button buttonFour;
    public Question[] questions;
    public Question currentQuestion;

    public int currentRound;

    public int rightAnswers;
    public int wrongAnswers;

    private Random random = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        roundCounter = findViewById(R.id.roundCounter);
        wrongAnswersCount = findViewById(R.id.wrongAnswersCount);
        rightAnswersCount = findViewById(R.id.rightAnswersCount);
        mainExample = findViewById(R.id.mainExample);

        buttonOne = findViewById(R.id.buttonOne);
        buttonTwo = findViewById(R.id.buttonTwo);
        buttonThree = findViewById(R.id.buttonThree);
        buttonFour = findViewById(R.id.buttonFour);

        buttonOne.setOnClickListener(this);
        buttonTwo.setOnClickListener(this);
        buttonThree.setOnClickListener(this);
        buttonFour.setOnClickListener(this);
        generateFirstRound();
    }

    private void generateFirstRound() {
        questions = Question.generateExamples();
        currentQuestion = questions[0];

        setRoundCounterText();
        setRightsAndWrongQuestionCounterText();
        setCurrentExampleText();
        setButtons();
    }


    private void setButtons() {
        Button[] buttons = new Button[]{buttonOne, buttonTwo, buttonThree, buttonFour};
        int randomInt = random.nextInt(buttons.length);
        Button randomRightAnswerButton = buttons[randomInt];

        if (currentQuestion.getRightResult() == (int) currentQuestion.getRightResult()) {
            randomRightAnswerButton.setText(String.format("%s", (int) currentQuestion.getRightResult()));
        } else {
            randomRightAnswerButton.setText(String.format("%s", currentQuestion.getRightResult()));
        }

        for (int i = 0; i < buttons.length; i++) {
            if (i != randomInt) {
                buttons[i].setText(String.format("%s", currentQuestion.getWrongAnswer()));
            }
        }
    }

    private void setCurrentExampleText() {
        mainExample.setText(currentQuestion.getExampleString());
    }

    private void setRightsAndWrongQuestionCounterText() {
        rightAnswersCount.setText(String.format("%s", rightAnswers));
        wrongAnswersCount.setText(String.format("%s", wrongAnswers));
    }

    private void setRoundCounterText() {
        currentRound++;
        roundCounter.setText(String.format("%s/%s", currentRound, questions.length));
    }

    @Override
    public void onClick(View v) {
        float number;

        switch (v.getId()) {
            case R.id.buttonOne:
                Log.d(TAG, "buttonOne click");
                number = Float.parseFloat(buttonOne.getText().toString());
                checkAnswer(number);
                break;
            case R.id.buttonTwo:
                Log.d(TAG, "buttonTwo click");
                number = Float.parseFloat(buttonTwo.getText().toString());
                checkAnswer(number);
                break;
            case R.id.buttonThree:
                Log.d(TAG, "buttonThree click");
                number = Float.parseFloat(buttonThree.getText().toString());
                checkAnswer(number);
                break;
            case R.id.buttonFour:
                Log.d(TAG, "buttonFour click");
                number = Float.parseFloat(buttonFour.getText().toString());
                checkAnswer(number);
                break;
        }
        nextRound();
    }

    private void nextRound() {
        if (currentRound < questions.length) {
            currentQuestion = questions[currentRound];
            setRoundCounterText();
            setRightsAndWrongQuestionCounterText();
            setCurrentExampleText();
            setButtons();
        } else {
            int percentOfRightAnswers = (100 / questions.length) * rightAnswers;
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            AlertDialog alert = builder
                    .setTitle("Вы ответили на все уравнения")
                    .setMessage(String.format("Процент правильных ответов %s %%", percentOfRightAnswers))
                    .setPositiveButton("Ещё", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            recreate();
                        }
                    })
                    .setNegativeButton("Закрыть", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }).create();
            alert.show();
        }
    }


    private void checkAnswer(float answer) {
        if (currentQuestion.getRightResult() == answer) {
            rightAnswers++;
        } else {
            wrongAnswers++;
        }

    }
}
